package Entity;

import Entity.TontPaises;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-01-21T19:22:29")
@StaticMetamodel(TontAves.class)
public class TontAves_ { 

    public static volatile SingularAttribute<TontAves, String> dsnombreComun;
    public static volatile SingularAttribute<TontAves, String> dsnombreCientifico;
    public static volatile CollectionAttribute<TontAves, TontPaises> tontPaisesCollection;
    public static volatile SingularAttribute<TontAves, String> cdave;

}