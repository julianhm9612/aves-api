package Entity;

import Entity.TontAves;
import Entity.TontZonas;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-01-21T19:22:29")
@StaticMetamodel(TontPaises.class)
public class TontPaises_ { 

    public static volatile SingularAttribute<TontPaises, TontZonas> cdzona;
    public static volatile CollectionAttribute<TontPaises, TontAves> tontAvesCollection;
    public static volatile SingularAttribute<TontPaises, String> cdpais;
    public static volatile SingularAttribute<TontPaises, String> dsnombre;

}